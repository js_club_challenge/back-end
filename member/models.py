from django.db import models
from django.template.defaultfilters import slugify
# Create your models here.

class Member(models.Model):
    name = models.CharField(max_length=100)
    avatar = models.CharField(max_length=200, blank=True)
    nickname = models.CharField(max_length=100, blank=True)
    detail = models.CharField(max_length=2000, blank=True)
    gen = models.IntegerField(default=10)
    student_id = models.CharField(max_length=8)

