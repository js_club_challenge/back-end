from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from member.api.serializers import *

class GetDetailMemberAPIViews(APIView):
    def get(self, request, pk):
        member = Member.objects.get(id=pk)
        serializer = GetDetailMemberSerializer(member)
        return Response(serializer.data)

class GetAllMemberAPIViews(APIView):

    def get(self, request):
        list_member = Member.objects.all()
        mydata = GetDetailMemberSerializer(list_member, many = True)
        return Response(data = mydata.data, status = status.HTTP_200_OK)

    def post(self, request):
        mydata = GetAllMemberSerializer(data=request.data)
        if mydata.is_valid():
            mydata.save()
            return Response(mydata.data, status=status.HTTP_201_CREATED)
        else:
            return Response("Error")



