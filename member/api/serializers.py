
from member.models import *
from rest_framework import serializers
# from django.template.defaultfilters import slugify

class GetAllMemberSerializer(serializers.ModelSerializer):

    class Meta:
        model = Member
        fields = ('name','avatar','nickname','detail','gen', 'student_id')

class GetDetailMemberSerializer(serializers.ModelSerializer):

    class Meta:
        model = Member
        fields = ('id','name','avatar','nickname','detail','gen', 'student_id')
