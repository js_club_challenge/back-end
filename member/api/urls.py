from django.urls import include, path
from rest_framework import routers
from rest_framework_simplejwt.views import TokenRefreshView
from member.views import *

router = routers.DefaultRouter()

urlpatterns = [
    path('', GetAllMemberAPIViews.as_view()),
    path('<int:pk>', GetDetailMemberAPIViews.as_view()),

]