from django.urls import reverse_lazy
from django.views.generic import UpdateView
from .forms import ProfileForm
from django.shortcuts import render,redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib import messages
from .models import *
from django.contrib.auth import authenticate,login
from .helpers import send_forget_password_mail

# Edit Profile View
class ProfileView(UpdateView):
    model = User
    form_class = ProfileForm
    success_url = reverse_lazy('home')
    template_name = 'commons/profile.html'

def home_view(request):
    context = {}
    return render(request, "home_profile.html", context)

def LoginView(request):
    try:
        if request.method == 'POST':
            username = request.POST.get('username')
            password = request.POST.get('password')
            
            if not username or not password:
                messages.success(request, 'Both Username and Password are required.')
                return redirect('/accounts/login/')
            user_obj = User.objects.filter(username = username).first()
            if user_obj is None:
                messages.success(request, 'User not found.')
                return redirect('/accounts/login/')
        
        
            user = authenticate(username = username , password = password)
            
            if user is None:
                messages.success(request, 'Wrong password.')
                return redirect('/accounts/login/')
        
            login(request , user)
            return redirect('/accounts/profile/')            
    except Exception as e:
        print(e)
    return render(request , 'commons/login.html')



def SignUpView(request):
    try:
        if request.method == 'POST':
            username = request.POST.get('username')
            email = request.POST.get('email')
            first_name = request.POST.get('first_name')
            last_name = request.POST.get('last_name')
            password = request.POST.get('password1')
            confirm_password = request.POST.get('password2')

        try:
            if User.objects.filter(username = username).first():
                messages.success(request, 'Username is taken.')
                return redirect('/accounts/signup/')

            if User.objects.filter(email = email).first():
                messages.success(request, 'Email is taken.')
                return redirect('/accounts/signup/')

            if  password != confirm_password:
                messages.success(request, 'Password confirm is incorrect.')
                return redirect('/accounts/signup/')                            
            
            user_obj = User(username = username , email = email, first_name = first_name, last_name = last_name)
            user_obj.set_password(password)
            user_obj.save()
    
            return redirect('/accounts/login/')

        except Exception as e:
            print(e)

    except Exception as e:
            print(e)

    return render(request , 'commons/signup.html')


@login_required(login_url='/accounts/login/')
def Home(request):
    return render(request , 'home_profile.html')



def ChangePassword(request , token):
    context = {}        
    try:
        profile_obj = Profile.objects.filter(forget_password_token = token).first()
        context = {'user_id' : profile_obj.user.id}
        
        if request.method == 'POST':
            new_password = request.POST.get('new_password')
            confirm_password = request.POST.get('reconfirm_password')
            user_id = request.POST.get('user_id')
            
            if user_id is  None:
                messages.success(request, 'No user id found.')
                return redirect(f'/accounts/change-password/{token}/')
                
            
            if  new_password != confirm_password:
                messages.success(request, 'both should  be equal.')
                return redirect(f'/accounts/change-password/{token}/')
                         
            
            user_obj = User.objects.get(id = user_id)
            user_obj.set_password(new_password)
            user_obj.save()
            return redirect('/accounts/login/')       
    except Exception as e:
        print(e)
    return render(request , 'commons/change-password.html' , context)


import uuid
def ForgetPassword(request):
    try:
        if request.method == 'POST':
            username = request.POST.get('username')
            
            if not User.objects.filter(username=username).first():
                messages.success(request, 'Not user found with this username.')
                return redirect('/accounts/forget-password/')
            
            user_obj = User.objects.get(username = username)
            token = str(uuid.uuid4())
            profile_obj= Profile.objects.get(user = user_obj)
            profile_obj.forget_password_token = token
            profile_obj.save()
            send_forget_password_mail(user_obj.email , token)
            messages.success(request, 'An email is sent.')
            return redirect('/accounts/login/')   
    except Exception as e:
        print(e)
    return render(request , 'commons/forget-password.html')    