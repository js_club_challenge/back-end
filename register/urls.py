from django.urls import path
from . import views
from .views import ProfileView
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('signup/', views.SignUpView, name='signup'),
    path('profile/', views.home_view, name='home'),    
    path('profile/<int:pk>/', ProfileView.as_view(), name='profile'),    
    path('login/', views.LoginView, name='login'),
    path('logout/', auth_views.LogoutView.as_view(next_page='home'), name='logout'),
    path('forget-password/' , views.ForgetPassword , name='password_reset'),
    path('change-password/<token>/' , views.ChangePassword, name='change_password'),    
    path(
        'change-password-profile/',
        auth_views.PasswordChangeView.as_view(
            template_name='commons/change_password_profile.html',                        
            success_url = '/accounts/profile/'            
        ),
        name='change_password_profile'
    ),
]